package com.cardgame.evolution.DisplayLogic.Room.VoteStart;

import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Common.SimpleObservable;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoomUser;
import com.cardgame.evolution.Logic.Room.CreateRoom.RoomStatus;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Observable;
import java.util.Observer;

public class VoteStartHandler implements Handleable<VoteStartRequest, Void> {

    @Override
    public Void handle(VoteStartRequest model) {
        DatabaseReference roomRef = FirebaseDatabase.getInstance().getReference()
                .child("rooms").child(model.roomKey);
        FirebaseObservable<FirebaseRoom> roomObservable = new FirebaseObservable<>(roomRef, FirebaseRoom.class);

        Observer roomObserver = (o, arg) -> {
            if(o == roomObservable) {
                roomObservable.deleteObservers();
                FirebaseRoom room = roomObservable.getValue();
                for (FirebaseRoomUser member : room.members) {
                    if (member.uid.equals(model.userUid)) {
                        member.voted = true;
                    }
                }

                room.votes = (int)room.members.stream().filter(x -> x.voted).count();
                if (room.votes == room.members.size()) {
                    room.roomStatus = RoomStatus.Starting;
                }

                roomObservable.setValue(room);
            }
        };

        roomObservable.addObserver(roomObserver);
        return null;
    }
}