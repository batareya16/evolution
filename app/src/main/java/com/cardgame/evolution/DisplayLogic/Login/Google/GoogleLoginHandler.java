package com.cardgame.evolution.DisplayLogic.Login.Google;

import com.cardgame.evolution.Common.Logger.Logger;
import com.cardgame.evolution.Common.Mediator.Handleable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;


import androidx.annotation.NonNull;

public class GoogleLoginHandler implements Handleable<GoogleLoginRequest, Task<AuthResult>> {

    @Override
    public Task<AuthResult> handle(GoogleLoginRequest model) {
        Logger.debug(this.getClass(), "starting google auth flow:" + model.acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(model.acct.getIdToken(), null);
        return model.firebaseAuth.signInWithCredential(credential);
    }
}
