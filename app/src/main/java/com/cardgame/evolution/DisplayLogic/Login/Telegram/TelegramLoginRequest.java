package com.cardgame.evolution.DisplayLogic.Login.Telegram;

import android.app.Activity;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = TelegramLoginHandler.class)
public class TelegramLoginRequest extends Request {

    Activity activity;

    public TelegramLoginRequest(Activity activity) {
        this.activity = activity;
    }
}
