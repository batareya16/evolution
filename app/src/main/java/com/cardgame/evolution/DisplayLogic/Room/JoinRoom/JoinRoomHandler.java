package com.cardgame.evolution.DisplayLogic.Room.JoinRoom;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.Common.SimpleObservable;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoomUser;
import com.cardgame.evolution.Logic.Room.LeaveRooms.LeaveRoomRequest;
import com.cardgame.evolution.Logic.Room.RoomStorage.GetRoom.GetRoomStorageRequest;
import com.cardgame.evolution.Logic.Room.RoomStorage.GetRoom.StorageRoom;
import com.cardgame.evolution.Logic.Room.RoomStorage.SetRoom.SetRoomStorageRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Observable;
import java.util.Observer;

public class JoinRoomHandler implements Handleable<JoinRoomRequest, Observable> {

    @Override
    public Observable handle(JoinRoomRequest model) {
        DatabaseReference roomRef = FirebaseDatabase.getInstance().getReference()
                .child("rooms").child(model.roomKey);
        FirebaseObservable<FirebaseRoom> firebaseObservable = new FirebaseObservable<>(roomRef, FirebaseRoom.class);

        Observable joinRoomObservable = new SimpleObservable();
        Observer firebaseObserver = (o, arg) -> {
            if(o == firebaseObservable) {
                FirebaseRoom room = firebaseObservable.getValue();
                room.key = model.roomKey;
                if (room.members.size() < room.maxPlayers) {
                    firebaseObservable.deleteObservers();
                    leaveOtherAndJoin(room, model.user, model.roomSharedPrefs);
                    firebaseObservable.setValue(room);
                    joinRoomObservable.notifyObservers();
                }
            }
        };

        firebaseObservable.addObserver(firebaseObserver);
        return joinRoomObservable;
    }


    private void leaveOtherAndJoin(FirebaseRoom currRoom, FirebaseRoomUser currUser, SharedPreferences roomSharedPrefs) {
        StorageRoom storageRoom = (StorageRoom) Mediator.send(new GetRoomStorageRequest(roomSharedPrefs));

        if (storageRoom != null && storageRoom.key != null && storageRoom.key.equals(currRoom.key))
        {
            return;
        }

        if (storageRoom != null && storageRoom.key != null)
        {
            Mediator.send(new LeaveRoomRequest(currUser.uid, storageRoom.key));
        }

        currRoom.members.add(currUser);
        Mediator.send(new SetRoomStorageRequest(currRoom.key, roomSharedPrefs));
    }
}
