package com.cardgame.evolution.DisplayLogic.Room.FindRoom;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = FindRoomHandler.class)
public class FindRoomRequest extends Request {
    public FindRoomRequest(String roomKey) {
        this.roomKey = roomKey;
    }

    String roomKey;
}
