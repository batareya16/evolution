package com.cardgame.evolution.DisplayLogic.Mappers;

import android.content.ClipData;

import com.cardgame.evolution.Display.Activities.Game.DragAndDrop.EntityDragDropData;

public final class ClipDataEntityDragDropMapper {
    public static EntityDragDropData Map(ClipData clipData) {
        return new EntityDragDropData(
                Boolean.parseBoolean(clipData.getItemAt(1).getText().toString())
        );
    }

    public static ClipData Map(boolean containsBadProp) {
        ClipData data = ClipData.newPlainText(EntityDragDropData.class.getName(), EntityDragDropData.class.getName());
        data.addItem(new ClipData.Item(String.valueOf(containsBadProp)));
        return data;
    }
}
