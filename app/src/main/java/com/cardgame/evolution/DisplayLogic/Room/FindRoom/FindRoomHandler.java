package com.cardgame.evolution.DisplayLogic.Room.FindRoom;

import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FindRoomHandler implements Handleable<FindRoomRequest, FirebaseObservable> {
    @Override
    public FirebaseObservable handle(FindRoomRequest model) {
        DatabaseReference roomRef = FirebaseDatabase.getInstance().getReference()
                .child("rooms").child(model.roomKey);
        return new FirebaseObservable<>(roomRef, FirebaseRoom.class);
    }
}
