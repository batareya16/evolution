package com.cardgame.evolution.DisplayLogic.Login.UserStorage.SaveUser;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Constants.Login.UserStorage;
import com.cardgame.evolution.Common.Mediator.Handleable;

import static com.cardgame.evolution.Common.StorageHelper.push;

public class SaveUserStorageHandler implements Handleable<SaveUserStorageRequest, Object> {

    @Override
    public Object handle(SaveUserStorageRequest model) {
        SharedPreferences sharedPreferences = model.sharedPreferences;
        push(sharedPreferences, UserStorage.NAME_KEY, model.user.getDisplayName());
        push(sharedPreferences, UserStorage.PHOTO_URI, model.user.getPhotoUrl().toString());
        push(sharedPreferences, UserStorage.EMAIL, model.user.getEmail());
        push(sharedPreferences, UserStorage.UID, model.user.getUid());
        return null;
    }
}
