package com.cardgame.evolution.DisplayLogic.Room.VoteStart;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = VoteStartHandler.class)
public class VoteStartRequest extends Request {
    public VoteStartRequest(String roomKey, String userUid) {
        this.roomKey = roomKey;
        this.userUid = userUid;
    }

    public String roomKey;
    public String userUid;
}
