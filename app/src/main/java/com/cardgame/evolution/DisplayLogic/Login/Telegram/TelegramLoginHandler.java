package com.cardgame.evolution.DisplayLogic.Login.Telegram;

import com.cardgame.evolution.Common.Constants.Login.TelegramAuth;
import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.Logic.Login.Telegram.GenerateTelegramLoginRequest;

import org.telegram.passport.TelegramPassport;

public class TelegramLoginHandler implements Handleable<TelegramLoginRequest, Object> {

    @Override
    public Object handle(TelegramLoginRequest model) {
        GenerateTelegramLoginRequest generateRequest = new GenerateTelegramLoginRequest();
        TelegramPassport.AuthRequest auth = (TelegramPassport.AuthRequest)Mediator.send(generateRequest);
        TelegramPassport.request(model.activity, auth, TelegramAuth.ACTIVITY_REQUEST_CODE);
        return null;
    }
}
