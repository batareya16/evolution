package com.cardgame.evolution.DisplayLogic.Room.CreateRoom;

import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.Logic.Room.CreateRoom.NewRoomFirebaseRequest;
import com.cardgame.evolution.Logic.Room.CreateRoom.NewRoomFirebaseResult;
import com.cardgame.evolution.Logic.Room.LeaveRooms.LeaveRoomRequest;
import com.cardgame.evolution.Logic.Room.RoomStorage.GetRoom.GetRoomStorageRequest;
import com.cardgame.evolution.Logic.Room.RoomStorage.GetRoom.StorageRoom;
import com.cardgame.evolution.Logic.Room.RoomStorage.SetRoom.SetRoomStorageRequest;
import com.google.android.gms.tasks.Task;

public class CreateRoomHandler implements Handleable<CreateRoomRequest, Task<Void>> {
    @Override
    public Task<Void> handle(CreateRoomRequest model) {
        StorageRoom storageRoom = (StorageRoom)Mediator.send(new GetRoomStorageRequest(model.roomSharedPrefs));
        if(storageRoom.key != null) {
            Mediator.send(new LeaveRoomRequest(model.hostUser.uid, storageRoom.key));
        }

        NewRoomFirebaseResult newRoom = (NewRoomFirebaseResult)Mediator.send(new NewRoomFirebaseRequest(
                model.name,
                model.password,
                model.hostUser,
                model.maxPlayers));

        Mediator.send(new SetRoomStorageRequest(newRoom.key, model.roomSharedPrefs));
        return newRoom.addRoomPromise;
    }
}
