package com.cardgame.evolution.DisplayLogic.Game.StartGame;

import android.app.ProgressDialog;
import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

import java.util.Observable;

@ResolveMediator(handler = StartGameHandler.class)
public class StartGameRequest extends Request {

    public StartGameRequest(
            String roomKey,
            SharedPreferences userSharedPrefs,
            ProgressDialog loadingDialog,
            Observable finishStartingObservable) {
        this.roomKey = roomKey;
        this.userSharedPrefs = userSharedPrefs;
        this.loadingDialog = loadingDialog;
        this.finishStartingObservable = finishStartingObservable;
    }

    public String roomKey;
    public SharedPreferences userSharedPrefs;
    public ProgressDialog loadingDialog;
    public Observable finishStartingObservable;
}
