package com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = GetUserStorageHandler.class)
public class GetUserStorageRequest extends Request {

    public GetUserStorageRequest(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public SharedPreferences sharedPreferences;
}
