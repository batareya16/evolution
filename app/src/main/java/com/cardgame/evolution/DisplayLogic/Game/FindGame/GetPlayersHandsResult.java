package com.cardgame.evolution.DisplayLogic.Game.FindGame;

import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Entities.Player.Player;

import java.util.List;
import java.util.Observable;

public class GetPlayersHandsResult {
    public GetPlayersHandsResult(List<FirebaseObservable<Player>> playersObservables, Observable playersGotFromDb) {
        this.playersObservables = playersObservables;
        this.playersGotFromDb = playersGotFromDb;
    }

    public List<FirebaseObservable<Player>> playersObservables;
    public Observable playersGotFromDb;
}
