package com.cardgame.evolution.DisplayLogic.Room.SearchRooms;

import com.cardgame.evolution.Common.FirebaseCollectionObservable;
import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class SearchRoomsHandler implements Handleable<SearchRoomsRequest, FirebaseCollectionObservable<FirebaseRoom>> {
    @Override
    public FirebaseCollectionObservable<FirebaseRoom> handle(SearchRoomsRequest model) {
        Query roomsRef = FirebaseDatabase.getInstance().getReference()
                .child("rooms")
                .orderByChild("name")
                .startAt(model.name.toUpperCase())
                .endAt(model.name.toLowerCase() + "\uf8ff")
                .limitToFirst(20);

        return new FirebaseCollectionObservable<>(roomsRef, FirebaseRoom.class);
    }
}
