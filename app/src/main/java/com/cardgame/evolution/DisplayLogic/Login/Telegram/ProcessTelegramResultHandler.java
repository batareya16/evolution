package com.cardgame.evolution.DisplayLogic.Login.Telegram;

import com.cardgame.evolution.Common.Constants.Login.TelegramAuth;
import com.cardgame.evolution.Common.Mediator.Handleable;

public class ProcessTelegramResultHandler implements Handleable<ProcessTelegramResultRequest, TelegramAuthenticationResult> {

    @Override
    public TelegramAuthenticationResult handle(ProcessTelegramResultRequest model) {
        if (model.requestCode == TelegramAuth.ACTIVITY_REQUEST_CODE) {

            if (model.resultCode == TelegramAuth.RESULT_OK_CODE) {
                return new TelegramAuthenticationResult();
            }
        }

        return null;
    }
}
