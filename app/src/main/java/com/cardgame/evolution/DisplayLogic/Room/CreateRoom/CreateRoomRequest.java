package com.cardgame.evolution.DisplayLogic.Room.CreateRoom;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.StorageUser;

@ResolveMediator(handler = CreateRoomHandler.class)
public class CreateRoomRequest extends Request {
    public CreateRoomRequest(
            String name,
            String password,
            StorageUser hostUser,
            SharedPreferences roomSharedPrefs,
            int maxPlayers) {
        this.name = name;
        this.password = password;
        this.hostUser = hostUser;
        this.roomSharedPrefs = roomSharedPrefs;
        this.maxPlayers = maxPlayers;
    }

    public String name;
    public String password;
    public int maxPlayers;
    public StorageUser hostUser;
    public SharedPreferences roomSharedPrefs;
}
