package com.cardgame.evolution.DisplayLogic.Login.UserStorage.SaveUser;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;
import com.google.firebase.auth.FirebaseUser;

@ResolveMediator(handler = SaveUserStorageHandler.class)
public class SaveUserStorageRequest extends Request {

    public SaveUserStorageRequest(FirebaseUser user, SharedPreferences sharedPreferences) {
        this.user = user;
        this.sharedPreferences = sharedPreferences;
    }

    public FirebaseUser user;
    public SharedPreferences sharedPreferences;
}
