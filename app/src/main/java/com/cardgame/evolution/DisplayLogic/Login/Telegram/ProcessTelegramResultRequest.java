package com.cardgame.evolution.DisplayLogic.Login.Telegram;

import android.content.Intent;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = ProcessTelegramResultHandler.class)
public class ProcessTelegramResultRequest extends Request {

    public ProcessTelegramResultRequest(int requestCode, int resultCode, Intent data) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
    }

    int requestCode;

    int resultCode;

    Intent data;
}
