package com.cardgame.evolution.DisplayLogic.Game.FindGame;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = GetPlayersHandsHandler.class)
public class GetPlayersHandsRequest extends Request {
    public GetPlayersHandsRequest(String roomKey) {
        this.roomKey = roomKey;
    }

    String roomKey;
}
