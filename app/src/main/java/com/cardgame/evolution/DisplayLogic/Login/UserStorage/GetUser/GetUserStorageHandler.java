package com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Constants.Login.UserStorage;
import com.cardgame.evolution.Common.Mediator.Handleable;

import static com.cardgame.evolution.Common.StorageHelper.*;

public class GetUserStorageHandler implements Handleable<GetUserStorageRequest, StorageUser> {

    @Override
    public StorageUser handle(GetUserStorageRequest model) {
        SharedPreferences sharedPreferences = model.sharedPreferences;
        return new StorageUser(
                pop(sharedPreferences, UserStorage.NAME_KEY),
                pop(sharedPreferences, UserStorage.PHOTO_URI),
                pop(sharedPreferences, UserStorage.EMAIL),
                pop(sharedPreferences, UserStorage.UID)
        );
    }
}
