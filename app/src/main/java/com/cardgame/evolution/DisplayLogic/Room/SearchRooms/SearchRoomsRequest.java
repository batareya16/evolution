package com.cardgame.evolution.DisplayLogic.Room.SearchRooms;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = SearchRoomsHandler.class)
public class SearchRoomsRequest extends Request {

    public SearchRoomsRequest(String name) {
        this.name = name;
    }

    public String name;
}
