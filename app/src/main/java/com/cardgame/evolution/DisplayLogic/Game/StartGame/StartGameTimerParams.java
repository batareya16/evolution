package com.cardgame.evolution.DisplayLogic.Game.StartGame;

import java.util.Timer;

public class StartGameTimerParams {
    StartGameTimerParams() {
        this.waitHostTimer = null;
        this.currentHostIndex = -1;
        this.prevHost = "";
    }

    public String prevHost;
    public Timer waitHostTimer;
    public int currentHostIndex;
}
