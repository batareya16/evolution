package com.cardgame.evolution.DisplayLogic.Login.Google;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.firebase.auth.FirebaseAuth;

@ResolveMediator(handler = GoogleLoginHandler.class)
public class GoogleLoginRequest extends Request {

    public GoogleLoginRequest(GoogleSignInClient googleSignInClient, GoogleSignInAccount acct, FirebaseAuth firebaseAuth) {
        this.googleSignInClient = googleSignInClient;
        this.acct = acct;
        this.firebaseAuth = firebaseAuth;
    }

    public GoogleSignInClient googleSignInClient;
    public GoogleSignInAccount acct;
    public FirebaseAuth firebaseAuth;
}
