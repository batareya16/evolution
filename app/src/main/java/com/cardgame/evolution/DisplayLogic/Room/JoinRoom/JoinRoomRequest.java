package com.cardgame.evolution.DisplayLogic.Room.JoinRoom;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoomUser;

@ResolveMediator(handler = JoinRoomHandler.class)
public class JoinRoomRequest extends Request {
    public JoinRoomRequest(String roomKey, FirebaseRoomUser user, SharedPreferences roomSharedPrefs) {
        this.roomKey = roomKey;
        this.user = user;
        this.roomSharedPrefs = roomSharedPrefs;
    }

    public String roomKey;
    public FirebaseRoomUser user;
    public SharedPreferences roomSharedPrefs;
}
