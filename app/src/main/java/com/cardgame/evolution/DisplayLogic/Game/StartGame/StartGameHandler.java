package com.cardgame.evolution.DisplayLogic.Game.StartGame;

import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.GetUserStorageRequest;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.StorageUser;
import com.cardgame.evolution.Logic.Game.StartGame.InitializeGame.InitializeGameRequest;
import com.cardgame.evolution.Logic.Game.StartGame.SelectHost.SelectHostRequest;
import com.cardgame.evolution.Logic.Game.StartGame.WaitHost.WaitHostRequest;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;
import com.cardgame.evolution.Logic.Room.CreateRoom.RoomStatus;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;
import java.util.stream.Collectors;

public class StartGameHandler implements Handleable<StartGameRequest, Object> {

    @Override
    public Object handle(StartGameRequest model) {

        DatabaseReference roomRef = FirebaseDatabase.getInstance().getReference()
                .child("rooms").child(model.roomKey);
        StorageUser currentUser = (StorageUser)Mediator.send(new GetUserStorageRequest(model.userSharedPrefs));

        final StartGameTimerParams gameTimerParams = new StartGameTimerParams();
        FirebaseObservable<FirebaseRoom> roomObservable = new FirebaseObservable<>(roomRef, FirebaseRoom.class);

        roomObservable.addObserver((o, arg) -> {

            if(roomObservable.getValue().roomStatus != RoomStatus.Starting) {
                roomObservable.deleteObservers();
                model.finishStartingObservable.notifyObservers();
                return;
            }

            String host = roomObservable.getValue().host;
            List<String> members = roomObservable.getValue().members.stream().map(x -> x.uid).collect(Collectors.toList());
            if (host == null) {
                model.loadingDialog.setMessage("Setting up initial host...");
                Mediator.send(new SelectHostRequest(members, roomObservable, gameTimerParams));
            }
            else if(host.equals(currentUser.uid)) {
                model.loadingDialog.setMessage("Initialization game on your device...");
                Mediator.send(new InitializeGameRequest(roomObservable));
            }
            else {
                model.loadingDialog.setMessage("Waiting up user " + members.indexOf(host) + " for host the game...");
                Mediator.send(new WaitHostRequest(gameTimerParams, roomObservable, members));
            }
        });

        return null;
    }
}
