package com.cardgame.evolution.DisplayLogic.Game.FindGame;

import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.Common.SimpleObservable;
import com.cardgame.evolution.DisplayLogic.Room.FindRoom.FindRoomRequest;
import com.cardgame.evolution.Entities.Player.Player;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class GetPlayersHandsHandler implements Handleable<GetPlayersHandsRequest, Object> {
    @Override
    public Object handle(GetPlayersHandsRequest model) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference()
                .child("rooms").child(model.roomKey).child("game").child("players");
        List<FirebaseObservable<Player>> playersObservables = new ArrayList<>();

        FirebaseObservable<FirebaseRoom> roomRef = (FirebaseObservable<FirebaseRoom>) Mediator.send(new FindRoomRequest(model.roomKey));
        Observable finishObservable = new SimpleObservable();
        Observer observer = (observable, arg) -> {
            if(observable == roomRef) {
                int playersCount = roomRef.getValue().game.players.size();
                for (int i = 0; i < playersCount; i++) {
                    playersObservables.add(new FirebaseObservable<>(databaseReference.child(String.valueOf(i)), Player.class));
                }

                roomRef.deleteObservers();
                finishObservable.notifyObservers();
            }
        };

        roomRef.addObserver(observer);
        return new GetPlayersHandsResult(playersObservables, finishObservable);
    }
}
