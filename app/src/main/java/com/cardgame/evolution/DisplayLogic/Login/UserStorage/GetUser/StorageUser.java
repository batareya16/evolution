package com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser;

public class StorageUser {

    public StorageUser(String name, String photoUri, String email, String uid) {
        this.name = name;
        this.photoUri = photoUri;
        this.email = email;
        this.uid = uid;
    }

    public String name;
    public String photoUri;
    public String email;
    public String uid;
}
