package com.cardgame.evolution.Common.Constants.Login;

public final class TelegramAuth {

    public static final int BOT_ID = 971411333;

    public static final String BOT_PUBLIC_KEY =
            "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv9GO86I0OXFnLLgIYLh+\n" +
            "1vEk52MwuqXchP/hAvZNnNw7AWtMS4HbcwlpDHDceUxMAlizqNEEjc0FKhJ47HOB\n" +
            "1IqjTWnV6sRrsCmo3HtbzAzBXGI4xmDt2KcP2970ohVBePXZT+63EMNfXrw08eR0\n" +
            "kI5bK/S8PgbWeDO7myS5TCmplmkMU0H0snyi6G/pBj1ppk3A6LTVFI0TiywWiRvv\n" +
            "GOFKj/iMJqwJwchbnontUCSf6GN2rc5lTLyi0DrC0FFQlLkmb2vPyj1nTp+2uxTm\n" +
            "x2VKKozdrfx9RvJuVEDfezzgvAR9IGF0JOHqfLm+sf5tl5uORw2xsJ/KurPnVoTt\n" +
            "AQIDAQAB\n" +
            "-----END PUBLIC KEY-----";

    public static final int NONCE_SYMBOLS = 20;

    public static final int ACTIVITY_REQUEST_CODE = 80;

    public static final int RESULT_OK_CODE = -1;
}
