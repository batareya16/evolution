package com.cardgame.evolution.Common;

import com.cardgame.evolution.Entities.Card.Card;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Shuffler {
    public static List<Card> shuffleCards(List<Card> collection) {
        Random rnd = new Random();
        Card[] array = new Card[collection.size()];
        array = collection.toArray(array);
        for (int i = array.length - 1; i >= 0; i--) {
            int swapTo = rnd.nextInt(array.length);
            swap(array, i, swapTo);
        }

        return Arrays.stream(array).collect(Collectors.toList());
    }

    private static void swap(Object[] collection, int a, int b) {
        Object temp = collection[a];
        collection[a] = collection[b];
        collection[b] = temp;
    }
}
