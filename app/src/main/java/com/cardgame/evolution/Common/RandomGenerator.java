package com.cardgame.evolution.Common;

import java.security.SecureRandom;
import java.util.Base64;

public final class RandomGenerator {
    public static String generateSecureString(int symbols) {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[symbols];
        random.nextBytes(bytes);
        Base64.Encoder encoder = Base64.getUrlEncoder().withoutPadding();
        return encoder.encodeToString(bytes);
    }
}
