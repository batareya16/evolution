package com.cardgame.evolution.Common.Bitmap;

import android.content.Context;

public final class DrawableHelper {
    public static int getDrawableId (Context context, String name) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }
}
