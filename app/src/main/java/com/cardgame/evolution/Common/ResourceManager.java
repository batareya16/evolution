package com.cardgame.evolution.Common;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceManager {

    private ResourceBundle bundle;

    public ResourceManager(Locale locale, String packageName) {
        bundle = ResourceBundle.getBundle(packageName, locale);
    }

    public String getLocalized(String string) {
        return bundle.getString(string);
    }
}
