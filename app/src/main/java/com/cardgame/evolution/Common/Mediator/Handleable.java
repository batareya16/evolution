package com.cardgame.evolution.Common.Mediator;

public interface Handleable<TParam, TResult> {

    TResult handle (TParam model);

}
