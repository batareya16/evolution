package com.cardgame.evolution.Common;

import java.util.Observable;

public class SimpleObservable extends Observable {
    @Override
    public void notifyObservers() {
        setChanged();
        super.notifyObservers();
    }
}
