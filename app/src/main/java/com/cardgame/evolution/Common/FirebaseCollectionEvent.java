package com.cardgame.evolution.Common;

public enum  FirebaseCollectionEvent {
    ItemAdded,
    ItemRemoved,
    ItemChanged
}
