package com.cardgame.evolution.Common.Constants.Login;

public final class UserStorage {
    public static final String SHARED_PREFS_NAME = "User";
    public static final String NAME_KEY = "Name";
    public static final String PHOTO_URI = "Photo";
    public static final String EMAIL = "Email";
    public static final String UID = "UID";
}
