package com.cardgame.evolution.Common.Mediator;

import android.util.Log;

import java.util.Objects;

public class Mediator {
    public static Object send(Request model) {
        try {
            Handleable handler = (Handleable)Objects.requireNonNull(
                    model.getClass().getAnnotation(ResolveMediator.class))
                    .handler()
                    .newInstance();
            return handler.handle(model);
        } catch (Exception e) {
            Log.e("Mediator","Mediator couldn't send request of type " + model.getClass().getName());
            e.printStackTrace();
        }

        return null;
    }
}
