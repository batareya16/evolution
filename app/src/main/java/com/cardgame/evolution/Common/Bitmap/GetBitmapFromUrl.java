package com.cardgame.evolution.Common.Bitmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.cardgame.evolution.Common.SimpleObservable;
import com.google.firebase.database.annotations.NotNull;

import java.net.URL;
import java.util.Observable;

public class GetBitmapFromUrl extends AsyncTask<String, Void, Bitmap> {

    private Observable finishGetObservable;

    public GetBitmapFromUrl(SimpleObservable finishGetObservable) {
        this.finishGetObservable = finishGetObservable;
    }

    @Override
    protected Bitmap doInBackground(@NotNull String... src) {
        try {
            URL url = new URL(src[0]);
            return BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        finishGetObservable.notifyObservers();
    }
}
