package com.cardgame.evolution.Common.Logger;

import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;

public final class Logger {

    public static void info(Class self, String msg) {
        Log.i(getTag(self), msg);
    }

    public static void error(Class self, String msg) {
        Log.e(getTag(self), msg);
    }

    public static void warn(Class self, String msg) {
        Log.w(getTag(self), msg);
    }

    public static void wtf(Class self, String msg) {
        Log.wtf(getTag(self), msg);
    }

    public static void debug(Class self, String msg) {
        Log.d(getTag(self), msg);
    }

    private static String getTag(Class self) { return self.getName(); }
}
