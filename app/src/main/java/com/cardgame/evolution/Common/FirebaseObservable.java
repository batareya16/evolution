package com.cardgame.evolution.Common;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Observable;

import androidx.annotation.NonNull;

public class FirebaseObservable<TE> extends Observable {

    private final Class<TE> typeParameterClass;
    private DatabaseReference databaseReference;
    private TE value;

    public FirebaseObservable(DatabaseReference databaseReference, Class<TE> typeParameterClass) {

        this.databaseReference = databaseReference;
        this.typeParameterClass = typeParameterClass;
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                updateLocalValue(snapshot.getValue(typeParameterClass));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void setValue(TE obj) {
        databaseReference.setValue(value).addOnCompleteListener(task -> updateLocalValue(obj));
    }

    public void removeValue() {
        databaseReference.removeValue().addOnCompleteListener(task -> updateLocalValue(null));
    }

    public TE getValue() {
        return value;
    }

    private void updateLocalValue(TE obj) {
        value = obj;
        setChanged();
        notifyObservers();
    }

    public DatabaseReference getDatabaseReference() {
        return databaseReference;
    }
}
