package com.cardgame.evolution.Common;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class FirebaseCollectionObservable<TE extends FirebaseReference> extends Observable {

    private Query query;
    private List<TE> values = new ArrayList<>();
    TE lastChangedValue;
    FirebaseCollectionEvent lastEvent;


    public FirebaseCollectionObservable(Query query, Class<TE> typeParameterClass) {
        this.query = query;
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String prevChildKey) {
                TE newValue = dataSnapshot.getValue(typeParameterClass);
                newValue.key = dataSnapshot.getKey();
                values.add(newValue);
                lastChangedValue = newValue;
                lastEvent = FirebaseCollectionEvent.ItemAdded;
                setChanged();
                notifyObservers();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String prevChildKey) {
                TE changedValue = dataSnapshot.getValue(typeParameterClass);
                changedValue.key = dataSnapshot.getKey();
                values.replaceAll(x -> x.key.equals(changedValue.key) ? changedValue : x);
                lastChangedValue = changedValue;
                lastEvent = FirebaseCollectionEvent.ItemChanged;
                setChanged();
                notifyObservers();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                TE oldValue = dataSnapshot.getValue(typeParameterClass);
                oldValue.key = dataSnapshot.getKey();
                values.remove(oldValue);
                lastChangedValue = oldValue;
                lastEvent = FirebaseCollectionEvent.ItemRemoved;
                setChanged();
                notifyObservers();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String prevChildKey) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public List<TE> getValue() {
        return values;
    }

    public TE getLast() {
        return lastChangedValue;
    }

    public FirebaseCollectionEvent getLastEvent() {
        return lastEvent;
    }
}
