package com.cardgame.evolution.Common;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.Logic.Storage.SharedPrefs.Pop.PopStringPrefsRequest;
import com.cardgame.evolution.Logic.Storage.SharedPrefs.Push.PushStringPrefsRequest;

public final class StorageHelper {
    public static String pop(SharedPreferences sharedPreferences, String key) {
        return (String) Mediator.send(new PopStringPrefsRequest(sharedPreferences, key));
    }

    public static void push(SharedPreferences sharedPreferences, String key, String value) {
        Mediator.send(new PushStringPrefsRequest(sharedPreferences, key, value));
    }
}
