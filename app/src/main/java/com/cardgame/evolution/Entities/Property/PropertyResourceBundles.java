package com.cardgame.evolution.Entities.Property;

public enum PropertyResourceBundles {

    NAMES_BUNDLE("property-names"),
    DESCRIPTIONS_BUNDLE("property-descriptions"),
    DRAWER_IDS_BUNDLE("property-drawerNames");

    private String packageName;

    PropertyResourceBundles(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageName() {
        return packageName;
    }
}
