package com.cardgame.evolution.Entities.Card;

import com.cardgame.evolution.Entities.Property.Enums.EntityProperty;
import com.cardgame.evolution.Entities.Property.Enums.PairProperty;
import com.cardgame.evolution.Entities.Property.Enums.Synthesis;
import com.cardgame.evolution.Entities.Property.Property;
import com.cardgame.evolution.R;

import java.util.ArrayList;
import java.util.List;

public final class PackCreator {
    public static List<Card> getStandartCardsPack() {
        List<Card> cards = new ArrayList<>();
        pushInto(cards, EntityProperty.SharpVision, EntityProperty.Fat, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Grazing, EntityProperty.Fat, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Parasite, EntityProperty.Fat, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Parasite, EntityProperty.Predator, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Big, EntityProperty.Fat, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Burrowing, EntityProperty.Fat, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Mimicry, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Big, EntityProperty.Predator, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Hibernation, EntityProperty.Predator, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Hibernation, EntityProperty.Predator, R.drawable.card_back, 4);
        pushInto(cards, PairProperty.Symbiosis, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Scavenger, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Piracy, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Tail, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Camouflage, EntityProperty.Fat, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Toxic, EntityProperty.Predator, R.drawable.card_back, 4);
        pushInto(cards, PairProperty.Cooperation, EntityProperty.Predator, R.drawable.card_back, 4);
        pushInto(cards, PairProperty.Cooperation, EntityProperty.Fat, R.drawable.card_back, 4);
        pushInto(cards, PairProperty.Interaction, EntityProperty.Predator, R.drawable.card_back, 4);
        pushInto(cards, PairProperty.Cooperation, PairProperty.Fluke, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.InkCloud, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Swimmings, R.drawable.card_back, 8);
        pushInto(cards, EntityProperty.Fast, R.drawable.card_back, 4);
        pushInto(cards, EntityProperty.Fly, EntityProperty.Predator, R.drawable.card_back, 2);
        pushInto(cards, EntityProperty.Intelligence, EntityProperty.Fat, R.drawable.card_back, 2);
        pushInto(cards, EntityProperty.LiveBirth, EntityProperty.Swimmings, R.drawable.card_back, 2);
        pushInto(cards, EntityProperty.Fly, Synthesis.Thermosyntesis, R.drawable.card_back, 2);
        pushInto(cards, EntityProperty.Intelligence, Synthesis.Thermosyntesis, R.drawable.card_back, 2);
        pushInto(cards, EntityProperty.LiveBirth, Synthesis.Photosynthesis, R.drawable.card_back, 2);
        pushInto(cards, EntityProperty.Fly, Synthesis.Photosynthesis, R.drawable.card_back, 2);
        pushInto(cards, EntityProperty.Ambush, Synthesis.Photosynthesis, R.drawable.card_back, 2);
        pushInto(cards, EntityProperty.Ambush, EntityProperty.Swimmings, R.drawable.card_back, 2);
        pushInto(cards, EntityProperty.Metamorphosis, EntityProperty.Predator, R.drawable.card_back, 2);
        pushInto(cards, PairProperty.Fluke, EntityProperty.Fat, R.drawable.card_back, 2);
        pushInto(cards, PairProperty.Fluke, PairProperty.Interaction, R.drawable.card_back, 2);
        pushInto(cards, EntityProperty.Metamorphosis, Synthesis.Thermosyntesis, R.drawable.card_back, 2);
        return cards;
    }

    private static void pushInto(List<Card> cards, EntityProperty prop1, EntityProperty prop2, int drawable, int count) {
        List<EntityProperty> badProps = getBadProperties();
        for (int i = 0; i < count; i++) {
            cards.add(new Card(
                    drawable,
                    new Property(prop1, badProps.contains(prop1)),
                    new Property(prop2, badProps.contains(prop2))));
        }
    }

    private static void pushInto(List<Card> cards, PairProperty prop1, PairProperty prop2, int drawable, int count) {
        List<PairProperty> badProps = getBadPairProperties();
        for (int i = 0; i < count; i++) {
            cards.add(new Card(
                    drawable,
                    new Property(prop1, badProps.contains(prop1)),
                    new Property(prop2, badProps.contains(prop2))));
        }
    }

    private static void pushInto(List<Card> cards, EntityProperty prop, int drawable, int count) {
        List<EntityProperty> badProps = getBadProperties();
        for (int i = 0; i < count; i++) {
            cards.add(new Card(
                    drawable,
                    new Property(prop, badProps.contains(prop))));
        }
    }

    private static void pushInto(List<Card> cards, PairProperty prop, int drawable, int count) {
        List<PairProperty> badProps = getBadPairProperties();
        for (int i = 0; i < count; i++) {
            cards.add(new Card(
                    drawable,
                    new Property(prop, badProps.contains(prop))));
        }
    }

    private static void pushInto(List<Card> cards, PairProperty prop1, EntityProperty prop2, int drawable, int count) {
        List<PairProperty> badPairProps = getBadPairProperties();
        List<EntityProperty> badProps = getBadProperties();
        for (int i = 0; i < count; i++) {
            cards.add(new Card(
                    drawable,
                    new Property(prop1, badPairProps.contains(prop1)),
                    new Property(prop2, badProps.contains(prop2))));
        }
    }

    private static void pushInto(List<Card> cards, EntityProperty prop1, Synthesis prop2, int drawable, int count) {
        List<EntityProperty> badProps = getBadProperties();
        for (int i = 0; i < count; i++) {
            cards.add(new Card(
                    drawable,
                    new Property(prop1, badProps.contains(prop1)),
                    new Property(prop2, false)));
        }
    }

    private static List<EntityProperty> getBadProperties() {
        List<EntityProperty> result = new ArrayList<>();
        result.add(EntityProperty.Parasite);
        return result;
    }

    private static List<PairProperty> getBadPairProperties() {
        List<PairProperty> result = new ArrayList<>();
        result.add(PairProperty.Fluke);
        return result;
    }
}
