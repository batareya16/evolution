package com.cardgame.evolution.Entities.Property;

import java.util.Locale;
import java.util.ResourceBundle;

public class PropertyHelper {

    public static Object getEntityProperty(Property property) {
        switch (property.type)
        {
            case PAIR_PROPERTY:
                return property.pairProperty;
            case PROPERTY:
                return property.entityProperty;
            case SYNTHESIS:
                return property.synthesis;
        }

        return null;
    }

    public static String getName(Property property) {
        return getFromResources(property, PropertyResourceBundles.NAMES_BUNDLE.getPackageName());
    }

    public static String getDescription(Property property) {
        return getFromResources(property, PropertyResourceBundles.DESCRIPTIONS_BUNDLE.getPackageName());
    }

    public static String getDrawableName(Property property) {
        return getFromResources(property, PropertyResourceBundles.DRAWER_IDS_BUNDLE.getPackageName());
    }

    private static String getFromResources(Property property, String bundleName) {
        ResourceBundle bundle = ResourceBundle.getBundle(bundleName, Locale.getDefault());
        return bundle.getString(getEntityProperty(property).toString());
    }
}
