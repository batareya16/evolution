package com.cardgame.evolution.Entities.GameProcess;

public enum Seasons {
    GROWING,
    SETUP_FEED,
    FEED,
    EXTINCTION
}
