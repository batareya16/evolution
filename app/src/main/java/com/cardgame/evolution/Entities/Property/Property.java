package com.cardgame.evolution.Entities.Property;

import com.cardgame.evolution.Entities.Property.Enums.EntityProperty;
import com.cardgame.evolution.Entities.Property.Enums.PairProperty;
import com.cardgame.evolution.Entities.Property.Enums.Synthesis;

public class Property extends ConcretePropertyType {
    protected boolean isBad;

    public Property(EntityProperty entityProperty, boolean isBad) {
        super(entityProperty);
        this.isBad = isBad;
    }

    //Need for firebase right serializing
    public Property() {
    }

    public Property(Synthesis synthesis, boolean isBad) {
        super(synthesis);
        this.isBad = isBad;
    }

    public Property(PairProperty pairProperty, boolean isBad) {
        super(pairProperty);
        this.isBad = isBad;
    }

    public boolean getIsBad() {
        return isBad;
    }
}
