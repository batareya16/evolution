package com.cardgame.evolution.Entities.Player;

import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoomUser;

public class Player {
    public Player(PlayerHand hand, FirebaseRoomUser roomUser) {
        this.hand = hand;
        this.roomUser = roomUser;
    }

    public Player(FirebaseRoomUser roomUser) {
        this.roomUser = roomUser;
        hand = new PlayerHand();
    }

    public Player() {
    }

    public PlayerHand hand;
    public FirebaseRoomUser roomUser;
}
