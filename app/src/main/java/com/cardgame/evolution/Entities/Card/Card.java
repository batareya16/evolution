package com.cardgame.evolution.Entities.Card;

import com.cardgame.evolution.Common.FirebaseReference;
import com.cardgame.evolution.Entities.Property.Property;

import java.util.Arrays;
import java.util.List;

public class Card extends FirebaseReference {

    public List<Property> properties;
    public int drawable;

    public int getPropertiesCount() {
        return properties.size();
    }

    public int getDrawable() {
        return drawable;
    }

    public Card(int drawable, Property... properties) {

        this.properties = Arrays.asList(properties);
        this.drawable = drawable;
    }

    public Card() {}

    public boolean isHave(final Property property) {
        return this.properties.stream().anyMatch(x -> x.equals(property));
    }
}
