package com.cardgame.evolution.Entities.Property.Enums;

public enum PairProperty{
    Cooperation /*сотрудничество*/,
    Interaction /*взаимодействие*/,
    Fluke /*трематода*/,
    Symbiosis/*симбиоз*/
}
