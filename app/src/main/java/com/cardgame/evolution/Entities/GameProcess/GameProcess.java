package com.cardgame.evolution.Entities.GameProcess;

import com.cardgame.evolution.Entities.Card.Card;

import java.util.List;

public class GameProcess {

    public Seasons season;

    public int gameFood;

    public List<Card> cardPack;

    public List<Integer> cubes;

    public GameEvent gameEvent;
}
