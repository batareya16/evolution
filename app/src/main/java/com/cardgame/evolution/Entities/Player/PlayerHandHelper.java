package com.cardgame.evolution.Entities.Player;

import com.cardgame.evolution.Entities.Card.Card;
import com.cardgame.evolution.Entities.GameProcess.Game;

public class PlayerHandHelper {
    public static void takeCard (Game game, Player takeTo) {
        Card card = game.gameProcess.cardPack.get(game.gameProcess.cardPack.size() - 1);
        game.gameProcess.cardPack.remove(card);
        takeTo.hand.cards.add(card);
    }
}
