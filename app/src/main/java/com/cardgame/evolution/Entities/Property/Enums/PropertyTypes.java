package com.cardgame.evolution.Entities.Property.Enums;

public enum PropertyTypes {
    PAIR_PROPERTY,
    PROPERTY,
    SYNTHESIS
}
