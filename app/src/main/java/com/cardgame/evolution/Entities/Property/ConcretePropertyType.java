package com.cardgame.evolution.Entities.Property;

import com.cardgame.evolution.Entities.Property.Enums.EntityProperty;
import com.cardgame.evolution.Entities.Property.Enums.PairProperty;
import com.cardgame.evolution.Entities.Property.Enums.PropertyTypes;
import com.cardgame.evolution.Entities.Property.Enums.Synthesis;

public abstract class ConcretePropertyType {
    //Public for right firebase serializing
    public PropertyTypes type;
    public EntityProperty entityProperty;
    public Synthesis synthesis;
    public PairProperty pairProperty;

    ConcretePropertyType() {
    }

    ConcretePropertyType(EntityProperty entityProperty) {
        this.entityProperty = entityProperty;
        type = PropertyTypes.PROPERTY;
    }

    ConcretePropertyType(Synthesis synthesis) {
        this.synthesis = synthesis;
        type = PropertyTypes.SYNTHESIS;
    }

    ConcretePropertyType(PairProperty pairProperty) {
        this.pairProperty = pairProperty;
        type = PropertyTypes.PAIR_PROPERTY;
    }

    public PropertyTypes getType() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        ConcretePropertyType temp = (ConcretePropertyType)obj;

        if(temp.type == this.type){
            switch (type)
            {
                case PAIR_PROPERTY:
                    return this.pairProperty == temp.pairProperty;
                case PROPERTY:
                    return this.entityProperty == temp.entityProperty;
                case SYNTHESIS:
                    return this.synthesis == temp.synthesis;
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
