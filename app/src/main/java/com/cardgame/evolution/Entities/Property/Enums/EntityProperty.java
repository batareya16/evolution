package com.cardgame.evolution.Entities.Property.Enums;

public enum EntityProperty {
    Big /*большое*/,
    Burrowing /*норное*/,
    Mimicry /*мимикрия*/,
    Hibernation/*спячка*/,
    Scavenger/*падальщик*/,
    Piracy/*пиратство*/,
    Tail/*отбрасывание хвоста*/,
    Camouflage/*камуфляж*/,
    Parasite /*паразит*/,
    Toxic /*ядовитое*/,
    SharpVision /*острое зрение*/,
    Grazing /*топотун*/,
    InkCloud /*чернильное облако*/,
    Swimmings /*водоплавающее*/,
    Fast /*быстрое*/,
    Fly /*полет*/,
    Intelligence /*интеллект*/,
    LiveBirth /*живорождение*/,
    Ambush /*засада*/,
    Metamorphosis /*метаморфоза*/,
    Predator /*хищник*/,
    Fat /*жировой запас*/
}
