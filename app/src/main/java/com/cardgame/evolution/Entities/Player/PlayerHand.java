package com.cardgame.evolution.Entities.Player;

import com.cardgame.evolution.Entities.Card.Card;
import com.cardgame.evolution.Entities.Card.Entity;

import java.util.ArrayList;
import java.util.List;

public class PlayerHand {
    public PlayerHand() {
        cards = new ArrayList<>();
        entities = new ArrayList<>();
        score = 0;
    }

    public List<Card> cards;
    public List<Entity> entities;
    public int score;
}
