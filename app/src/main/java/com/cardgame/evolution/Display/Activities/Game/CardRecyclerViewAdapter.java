package com.cardgame.evolution.Display.Activities.Game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.cardgame.evolution.Display.Activities.Game.DragAndDrop.HandCardsDragManager;
import com.cardgame.evolution.Display.Activities.Game.PreviewCard.PreviewHandCardManager;
import com.cardgame.evolution.Entities.Card.Card;
import com.cardgame.evolution.R;

import java.util.List;

public class CardRecyclerViewAdapter extends RecyclerView.Adapter<CardRecyclerViewAdapter.CardsViewHolder> {
    private List<Card> mDataset;
    private Context context;
    private ConstraintLayout gameTableConstraintLayout;

    public class CardsViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private HandCardsDragManager dragManager;

        public CardsViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.player_hand_card_image);
            dragManager = new HandCardsDragManager(imageView);
        }

        void setDetails(Card card) {
            Bitmap image = BitmapFactory.decodeResource(context.getResources(), card.drawable);
            imageView.setImageBitmap(image);
            imageView.setOnLongClickListener(dragManager.getTouchEvent(card));
            PreviewHandCardManager pcm = new PreviewHandCardManager(context, gameTableConstraintLayout, card);
            imageView.setOnClickListener(v -> pcm.CreatePreview());
            imageView.setOnDragListener(dragManager.getOnDragListener());
        }
    }

    public CardRecyclerViewAdapter(
            List<Card> myDataset,
            Context context, ConstraintLayout gameTableConstraintLayout) {
        mDataset = myDataset;
        this.context = context;
        this.gameTableConstraintLayout = gameTableConstraintLayout;
    }

    @NonNull
    @Override
    public CardRecyclerViewAdapter.CardsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item_3, parent, false);
        return new CardRecyclerViewAdapter.CardsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardsViewHolder holder, int position) {
        holder.setDetails(mDataset.get(position));
    }

    public void addItem(Card item) {
        mDataset.add(item);
        notifyItemInserted(mDataset.size() - 1);
    }

    public void removeItem(Card item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(item);
        notifyItemRemoved(position);
    }

    public void changeItem(Card item) {
        int position = mDataset.indexOf(mDataset.stream().filter(x -> x.key.equals(item.key)).findFirst().get());
        mDataset.remove(position);
        mDataset.add(position, item);
        notifyItemChanged(position);
    }

    public void clearData() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
