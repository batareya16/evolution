package com.cardgame.evolution.Display.Activities.Game;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cardgame.evolution.Common.Bitmap.GetBitmapFromUrl;
import com.cardgame.evolution.Common.FirebaseCollectionObservable;
import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.SimpleObservable;
import com.cardgame.evolution.Entities.Card.Card;
import com.cardgame.evolution.Entities.Card.Entity;
import com.cardgame.evolution.Entities.Card.PackCreator;
import com.cardgame.evolution.Entities.Player.Player;
import com.cardgame.evolution.Entities.Property.Property;
import com.cardgame.evolution.R;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class PlayerSpaceController {

    private TableLayout tableLayout;
    private Context context;

    public PlayerSpaceController(TableLayout tableLayout, Context context) {
        this.tableLayout = tableLayout;
        this.context = context;
    }

    public void setup(View playerSpace, FirebaseObservable<Player> playerFirebaseObservable) {
        Player player = playerFirebaseObservable.getValue();
        setupPhoto(playerSpace.findViewById(R.id.player_space_photo), player);
        setupName(playerSpace.findViewById(R.id.player_space_player_name), player);
        setupCardsCount(playerSpace.findViewById(R.id.player_space_cards_info).findViewById(R.id.cards_count), player);
        setupScore(playerSpace.findViewById(R.id.player_space_score), player);
        playerSpace.setTag(new TableViewTag(TableItemTypes.PLAYER, player.roomUser.uid));

        DatabaseReference entitiesRef = playerFirebaseObservable.getDatabaseReference().child("hand").child("entities");
        FirebaseCollectionObservable<Entity> handEntitiesObservable = new FirebaseCollectionObservable<>(
                entitiesRef,
                Entity.class);
        entitiesRef.keepSynced(true);
        List<Card> standartCardsPack = PackCreator.getStandartCardsPack();
        setupEntities(playerSpace.findViewById(R.id.player_entities), Arrays.asList(standartCardsPack.get(0), standartCardsPack.get(1)));
    }

    private void setupScore(TextView scoreView, Player player) {
        scoreView.setText(String.format("Score: %d", player.hand.score));
    }

    private void setupCardsCount(TextView cardsCountView, Player player) {
        cardsCountView.setText(String.valueOf(player.hand.cards.size()));
    }

    private void setupEntities(RecyclerView entitiesView, Collection<Card> entities) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        entitiesView.setLayoutManager(layoutManager);
        EntityRecyclerViewAdapter adapter = new EntityRecyclerViewAdapter(new ArrayList<>(), context);
        for (Card entity : entities) {
            adapter.addItem(entity);
        }
        entitiesView.setAdapter(adapter);
    }

    private void setupName(TextView playerName, Player player) {
        playerName.setText(player.roomUser.userName);
    }

    private void setupPhoto(ImageView playerPhoto, Player player) {
        SimpleObservable playerPhotoObservable = new SimpleObservable();
        GetBitmapFromUrl bitmapFromUrl = new GetBitmapFromUrl(playerPhotoObservable);
        playerPhotoObservable.addObserver((o, arg) -> {
            try {
                playerPhoto.setImageBitmap(bitmapFromUrl.get());
            } catch (Exception ignored) {}
        });
        bitmapFromUrl.execute(player.roomUser.photoUri);
    }
}
