package com.cardgame.evolution.Display.Activities.Game;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Space;
import android.widget.TableLayout;
import android.widget.TableRow;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.DisplayLogic.Game.FindGame.GetPlayersHandsRequest;
import com.cardgame.evolution.DisplayLogic.Game.FindGame.GetPlayersHandsResult;
import com.cardgame.evolution.Entities.Player.Player;

public class TableInitializer {

    private TableLayout gameTable;
    private ConstraintLayout gameTableConstraintContainer;
    private Context context;
    private RecyclerView handCardsView;

    public TableInitializer(TableLayout gameTable, ConstraintLayout gameTableConstraintContainer, Context context, RecyclerView handCardsView) {
        this.gameTable = gameTable;
        this.gameTableConstraintContainer = gameTableConstraintContainer;
        this.context = context;
        this.handCardsView = handCardsView;
    }

    public void setupPlayers(String roomKey, String storageUserId) {
        GetPlayersHandsResult getPlayersHandsResult = (GetPlayersHandsResult) Mediator.send(
                new GetPlayersHandsRequest(roomKey));
        getPlayersHandsResult.playersGotFromDb.addObserver((o, arg) -> {
            int playersCount = getPlayersHandsResult.playersObservables.size();
            fillUpEmptyCellsWithSpaces(playersCount);
            fillUpPlayerCells(storageUserId, playersCount, getPlayersHandsResult);
        });
    }

    private void fillUpHandCards(RecyclerView handCardsView, Player currentPlayer) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        handCardsView.setLayoutManager(layoutManager);
        CardRecyclerViewAdapter adapter = new CardRecyclerViewAdapter(currentPlayer.hand.cards, context, gameTableConstraintContainer);
        handCardsView.setAdapter(adapter);
    }

    private void fillUpPlayerCells(String storageUserId, int playersCount, GetPlayersHandsResult getPlayersHandsResult) {
        final boolean[] wasMinePlayer = {false};
        for (int i = 0; i < playersCount; i++) {
            FirebaseObservable<Player> playerObservable = getPlayersHandsResult.playersObservables.get(i);
            int index = i;
            playerObservable.addObserver((o1, arg1) -> {
                if (o1 == playerObservable) {
                    Player player = playerObservable.getValue();
                    boolean isMinePlayer = player.roomUser.uid.equals(storageUserId);
                    View playerSpace = getPlayerCell(index, isMinePlayer, wasMinePlayer[0]);
                    wasMinePlayer[0] = isMinePlayer || wasMinePlayer[0];
                    PlayerSpaceController spaceController = new PlayerSpaceController(gameTable, context);
                    spaceController.setup(playerSpace, playerObservable);

                    if (isMinePlayer) {
                        fillUpHandCards(handCardsView, player);
                    }
                }
            });
        }
    }

    private void fillUpEmptyCellsWithSpaces(int playersCount) {
        for (int i = playersCount; i < 8; i++) {
            View playerSpace = getPlayerCell(i, false, true);
            Space emptySpace = new Space(context);
            emptySpace.setLayoutParams(new TableRow.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    1.0f));
            emptySpace.setMinimumHeight(-1);
            emptySpace.setMinimumWidth(-1);
            replaceView(playerSpace, emptySpace);
        }
    }

    private void replaceView(View from, View to) {
        ViewGroup parent = (ViewGroup) from.getParent();
        int index = parent.indexOfChild(from);
        parent.removeView(from);
        parent.addView(to, index);
    }

    private View getPlayerCell (int index, boolean isMine, boolean wasMine) {
        int resultIndex = index;
        if (isMine) {
            resultIndex = 0;
        } else if (!wasMine) {
            resultIndex++;
        }

        switch (resultIndex) {
            case 0: return getPlayersCellInner(2, 1);
            case 1: return getPlayersCellInner(0, 1);
            case 2: return getPlayersCellInner(1, 2);
            case 3: return getPlayersCellInner(1, 0);
            case 4: return getPlayersCellInner(0, 0);
            case 5: return getPlayersCellInner(0, 2);
            case 6: return getPlayersCellInner(2, 0);
            case 7: return getPlayersCellInner(2, 2);
            default: throw new IndexOutOfBoundsException();
        }
    }

    private View getPlayersCellInner (int row, int col) {
        return (((TableRow)gameTable.getChildAt(row))).getChildAt(col);
    }
}
