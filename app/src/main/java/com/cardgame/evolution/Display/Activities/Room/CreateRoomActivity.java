package com.cardgame.evolution.Display.Activities.Room;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.cardgame.evolution.Common.Constants.Login.UserStorage;
import com.cardgame.evolution.Common.Constants.Room.RoomStorage;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.GetUserStorageRequest;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.StorageUser;
import com.cardgame.evolution.DisplayLogic.Room.CreateRoom.CreateRoomRequest;
import com.cardgame.evolution.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import androidx.appcompat.app.AppCompatActivity;

public class CreateRoomActivity extends AppCompatActivity {

    StorageUser loggedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_room);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loggedUser = (StorageUser) Mediator.send(
                new GetUserStorageRequest(getSharedPreferences(UserStorage.SHARED_PREFS_NAME, MODE_PRIVATE)));
    }

    public void createRoomButtonClick(View view) {
        EditText roomName = findViewById(R.id.room_name);
        EditText password = findViewById(R.id.room_password);
        EditText maxPlayers = findViewById(R.id.room_maxplayers);

        Task<Void> createRoomPromise = (Task<Void>)Mediator.send(new CreateRoomRequest(
                roomName.getText().toString(),
                password.getText().toString(),
                loggedUser,
                getSharedPreferences(RoomStorage.SHARED_PREFS_NAME, MODE_PRIVATE),
                Integer.parseInt(maxPlayers.getText().toString())));
        ProgressDialog progressDialog = ProgressDialog.show(this, "Loading", "Creating room...");
        createRoomPromise.addOnSuccessListener(aVoid -> {
            Intent intent = new Intent(this, RoomHubActivity.class);
            progressDialog.dismiss();
            startActivity(intent);
        });
    }
}
