package com.cardgame.evolution.Display.Activities.Room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.cardgame.evolution.Common.Constants.Login.UserStorage;
import com.cardgame.evolution.Common.Constants.Room.RoomStorage;
import com.cardgame.evolution.Common.FirebaseCollectionObservable;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.Common.SimpleObservable;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.GetUserStorageRequest;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.StorageUser;
import com.cardgame.evolution.DisplayLogic.Room.SearchRooms.SearchRoomsRequest;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;
import com.cardgame.evolution.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class FindRoomActivity extends AppCompatActivity {

    FirebaseCollectionObservable<FirebaseRoom> roomsObservable;
    RecyclerView roomsCardView;
    Context context;
    List<FirebaseRoom> roomList;
    private RoomsRecyclerViewAdapter adapter;
    private EditText searchField;
    private Observable roomJoinedObservable;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_room);
        roomsCardView = findViewById(R.id.find_room_items);
        searchField = findViewById(R.id.search_room_text);
        context = this;
        roomJoinedObservable = new SimpleObservable();
        roomJoinedObservable.addObserver((o, arg) -> {
            if (o == roomJoinedObservable) {
                Intent intent = new Intent(this, RoomHubActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        roomList = new ArrayList<>();
        StorageUser currentStorageUser = (StorageUser) Mediator.send(new GetUserStorageRequest(
                getSharedPreferences(UserStorage.SHARED_PREFS_NAME, MODE_PRIVATE)));
        adapter = new RoomsRecyclerViewAdapter(
                roomList,
                this,
                currentStorageUser,
                roomJoinedObservable,
                getSharedPreferences(RoomStorage.SHARED_PREFS_NAME, MODE_PRIVATE));
        roomsCardView.setLayoutManager(new LinearLayoutManager(this));
        roomsCardView.setAdapter(adapter);
        sendSearchRequest("");
    }

    protected void sendSearchRequest(String text) {
        progressDialog = ProgressDialog.show(context, "Loading", "Loading available rooms...");
        roomsObservable = (FirebaseCollectionObservable<FirebaseRoom>)Mediator.send(new SearchRoomsRequest(text));
        Observer observer = (observable, arg) -> {
            if(observable == roomsObservable) {
                switch (roomsObservable.getLastEvent()) {
                    case ItemAdded: adapter.addItem(roomsObservable.getLast()); break;
                    case ItemRemoved: adapter.removeItem(roomsObservable.getLast()); break;
                    case ItemChanged: adapter.changeItem(roomsObservable.getLast()); break;
                }

                roomList = roomsObservable.getValue();
                progressDialog.dismiss();
            }
        };
        roomsObservable.addObserver(observer);
    }

    protected void onClickSearch(View view) {
        adapter.clearData();
        sendSearchRequest(searchField.getText().toString());
    }
}
