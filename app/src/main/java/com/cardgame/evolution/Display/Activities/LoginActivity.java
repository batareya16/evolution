package com.cardgame.evolution.Display.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.cardgame.evolution.Common.Constants.Login.GoogleAuth;
import com.cardgame.evolution.Common.Constants.Login.TelegramAuth;
import com.cardgame.evolution.Common.Constants.Login.UserStorage;
import com.cardgame.evolution.Common.Logger.Logger;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.DisplayLogic.Login.Google.GoogleLoginRequest;
import com.cardgame.evolution.DisplayLogic.Login.Telegram.ProcessTelegramResultRequest;
import com.cardgame.evolution.DisplayLogic.Login.Telegram.TelegramLoginRequest;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.SaveUser.SaveUserStorageRequest;
import com.cardgame.evolution.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    private GoogleSignInClient googleSignInClient;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        firebaseAuth = FirebaseAuth.getInstance();
        googleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    protected void onStart () {
        super.onStart();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) {
            goMainMenu(currentUser);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        checkTelegramActivityResult(requestCode, resultCode, data);
        checkGoogleActivityResult(requestCode, data);
    }

    public void telegramAuthenticate(View view) {
        TelegramLoginRequest loginRequest = new TelegramLoginRequest(this);
        Mediator.send(loginRequest);
    }

    public void googleAuthenticate(View view) {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GoogleAuth.RC_SIGN_IN);
    }

    public void goMainMenu(FirebaseUser loggedUser) {
        Mediator.send(new SaveUserStorageRequest(loggedUser, getSharedPreferences(UserStorage.SHARED_PREFS_NAME, MODE_PRIVATE)));
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }

    private void checkTelegramActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TelegramAuth.ACTIVITY_REQUEST_CODE) {
            Mediator.send(new ProcessTelegramResultRequest(requestCode, resultCode, data));
        }
    }

    private void checkGoogleActivityResult(int requestCode, Intent data) {
        if (requestCode == GoogleAuth.RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                GoogleLoginRequest loginRequest = new GoogleLoginRequest(
                        googleSignInClient,
                        account,
                        firebaseAuth);

                Task<AuthResult> googleLoginTask = (Task<AuthResult>)Mediator.send(loginRequest);
                googleLoginTask
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Logger.debug(this.getClass(), "google auth flow:success");
                                    goMainMenu(firebaseAuth.getCurrentUser());
                                } else {
                                    Logger.warn(this.getClass(), "google auth flow:failure" + task.getException().getMessage());
                                    Snackbar.make(findViewById(R.id.login_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                                }
                            }
                        });

            } catch (ApiException e) {
                Logger.wtf(this.getClass(), "Google sign in failed" + e.getMessage());
                e.printStackTrace();
            }
        }
    }
}
