package com.cardgame.evolution.Display.Activities.Game.PreviewCard;

import android.content.Context;
import android.graphics.Bitmap;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.cardgame.evolution.Entities.Card.Card;
import com.cardgame.evolution.Entities.Property.PropertyHelper;
import com.cardgame.evolution.R;

import java.util.List;
import java.util.stream.Collectors;

public class PreviewHandCardManager {

    private Context context;
    private ConstraintLayout gameTableConstraintLayout;
    private Card card;

    public PreviewHandCardManager(Context context, ConstraintLayout gameTableConstraintLayout, Card card) {
        this.context = context;
        this.gameTableConstraintLayout = gameTableConstraintLayout;
        this.card = card;
    }

    public void CreatePreview() {
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert vi != null;
        View v = vi.inflate(R.layout.game_center_preview_hand_card, null);

        //Fill details
        TextView textView = v.findViewById(R.id.hand_card_preview_description);
        List<String> propertyNames = card.properties.stream()
                .map(e -> String.format("%s \n %s", PropertyHelper.getName(e), PropertyHelper.getDescription(e)))
                .collect(Collectors.toList());
        textView.setText(String.join("\n\n", propertyNames));

        ImageView imageView = v.findViewById(R.id.hand_card_preview_photo);
        imageView.setImageResource(card.drawable);

        v.setZ(999999);
        Transition transition = new Slide(Gravity.BOTTOM);
        v.setOnClickListener(v1 -> {
            Transition closeTransition = new Slide(Gravity.BOTTOM);
            TransitionManager.beginDelayedTransition(gameTableConstraintLayout, closeTransition);
            ViewGroup parent = (ViewGroup) v1.getParent();
            parent.removeView(v1);
        });

        TransitionManager.beginDelayedTransition(gameTableConstraintLayout, transition);
        gameTableConstraintLayout.addView(v, 0, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
    }
}
