package com.cardgame.evolution.Display.Activities.Game.DragAndDrop;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.ImageView;

import com.cardgame.evolution.DisplayLogic.Mappers.ClipDataEntityDragDropMapper;
import com.cardgame.evolution.Entities.Card.Card;
import com.cardgame.evolution.Entities.Property.Property;

public class HandCardsDragManager {

    private ImageView cardView;

    public HandCardsDragManager(ImageView cardView) {
        this.cardView = cardView;
    }

    public View.OnLongClickListener getTouchEvent(Card card) {
        final boolean containsBadProp = card.properties.stream().anyMatch(Property::getIsBad);
        return (v) -> {
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(cardView);
            cardView.startDragAndDrop(ClipDataEntityDragDropMapper.Map(containsBadProp), shadowBuilder, cardView, 0);
            cardView.setVisibility(View.INVISIBLE);
            return true;
        };
    }

    public View.OnDragListener getOnDragListener() {
        return (v, event) -> true;
    }
}
