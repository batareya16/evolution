package com.cardgame.evolution.Display.Activities.Room;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cardgame.evolution.Common.Bitmap.GetBitmapFromUrl;
import com.cardgame.evolution.Common.SimpleObservable;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoomUser;
import com.cardgame.evolution.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PlayersRecyclerViewAdapter extends RecyclerView.Adapter<PlayersRecyclerViewAdapter.PlayersViewHolder> {
    private List<FirebaseRoomUser> mDataset;
    private Context context;

    public class PlayersViewHolder extends RecyclerView.ViewHolder {

        private TextView playerNameText, playerDescrText;
        private ImageView playerPhoto;

        public PlayersViewHolder(View itemView) {
            super(itemView);
            playerNameText = itemView.findViewById(R.id.player_name);
            playerDescrText = itemView.findViewById(R.id.player_description);
            playerPhoto = itemView.findViewById(R.id.player_photo);
        }

        void setDetails(FirebaseRoomUser player) {
            playerNameText.setText(player.userName);
            playerDescrText.setText(player.email);

            SimpleObservable playerPhotoObservable = new SimpleObservable();
            GetBitmapFromUrl bitmapFromUrl = new GetBitmapFromUrl(playerPhotoObservable);
            playerPhotoObservable.addObserver((o, arg) -> {
                try {
                    playerPhoto.setImageBitmap(bitmapFromUrl.get());
                } catch (Exception ignored) {}
            });
            bitmapFromUrl.execute(player.photoUri);
        }
    }

    public PlayersRecyclerViewAdapter(
            List<FirebaseRoomUser> users,
            Context context) {
        mDataset = users;
        this.context = context;
    }

    @NonNull
    @Override
    public PlayersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item_1, parent, false);
        return new PlayersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayersViewHolder holder, int position) {
        holder.setDetails(mDataset.get(position));
    }

    public void addItem(FirebaseRoomUser item) {
        mDataset.add(item);
        notifyItemInserted(mDataset.size() - 1);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}