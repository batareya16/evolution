package com.cardgame.evolution.Display.Activities.Room;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.StorageUser;
import com.cardgame.evolution.DisplayLogic.Room.JoinRoom.JoinRoomRequest;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoomUser;
import com.cardgame.evolution.R;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Locale;
import java.util.Observable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RoomsRecyclerViewAdapter extends RecyclerView.Adapter<RoomsRecyclerViewAdapter.RoomsViewHolder> {
    private final Observable roomJoinedObservable;
    private List<FirebaseRoom> mDataset;
    private Context context;
    private FirebaseRoomUser currentFirebaseUser;
    private SharedPreferences roomSharedPreferences;


    public class RoomsViewHolder extends RecyclerView.ViewHolder {

        private TextView roomNameView, membersCountView;
        private Observable joinRoomCoreObs;

        public RoomsViewHolder(View itemView) {
            super(itemView);
            roomNameView = itemView.findViewById(R.id.find_room_item_name);
            membersCountView = itemView.findViewById(R.id.find_room_item_members);
        }

        void setDetails(FirebaseRoom room) {
            roomNameView.setText(room.name);
            membersCountView.setText(String.format(Locale.US, "%d / %d", room.members.size(), room.maxPlayers));
            itemView.setOnClickListener(view -> toggleItemSelection(room));
        }

        private void toggleItemSelection(FirebaseRoom room) {
            if (room.maxPlayers <= room.members.size()) {
                Snackbar.make(itemView, "Can't join to full room", Snackbar.LENGTH_LONG).show();
                return;
            }

            ProgressDialog dialog = ProgressDialog.show(context, "Loading", "Joining into room...");
            joinRoomCoreObs = (Observable) Mediator.send(new JoinRoomRequest(room.key, currentFirebaseUser, roomSharedPreferences));
            joinRoomCoreObs.addObserver((o, arg) -> {
                if (o == joinRoomCoreObs) {
                    roomJoinedObservable.notifyObservers();
                    dialog.dismiss();
                }
            });
        }
    }

    public RoomsRecyclerViewAdapter(
            List<FirebaseRoom> myDataset,
            Context context,
            StorageUser storageUser,
            Observable roomJoinedObservable,
            SharedPreferences roomSharedPreferences) {
        mDataset = myDataset;
        this.context = context;
        currentFirebaseUser = new FirebaseRoomUser(storageUser.uid, storageUser.name, storageUser.email, storageUser.photoUri);
        this.roomJoinedObservable = roomJoinedObservable;
        this.roomSharedPreferences = roomSharedPreferences;
    }

    @NonNull
    @Override
    public RoomsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item, parent, false);
        return new RoomsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomsViewHolder holder, int position) {
        holder.setDetails(mDataset.get(position));
    }

    public void addItem(FirebaseRoom item) {
        mDataset.add(item);
        notifyItemInserted(mDataset.size() - 1);
    }

    public void removeItem(FirebaseRoom item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(item);
        notifyItemRemoved(position);
    }

    public void changeItem(FirebaseRoom item) {
        int position = mDataset.indexOf(mDataset.stream().filter(x -> x.key.equals(item.key)).findFirst().get());
        mDataset.remove(position);
        mDataset.add(position, item);
        notifyItemChanged(position);
    }

    public void clearData() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}