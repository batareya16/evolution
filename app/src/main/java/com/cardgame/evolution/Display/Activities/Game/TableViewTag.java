package com.cardgame.evolution.Display.Activities.Game;

public class TableViewTag {
    public TableItemTypes type;
    public String name;

    public TableViewTag(TableItemTypes type, String name) {
        this.type = type;
        this.name = name;
    }
}
