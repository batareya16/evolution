package com.cardgame.evolution.Display.Activities.Room;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cardgame.evolution.Common.Constants.Login.UserStorage;
import com.cardgame.evolution.Common.Constants.Room.RoomStorage;
import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.Common.SimpleObservable;
import com.cardgame.evolution.Display.Activities.Game.GameTableActivity;
import com.cardgame.evolution.Display.Activities.MainMenuActivity;
import com.cardgame.evolution.DisplayLogic.Game.StartGame.StartGameRequest;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.GetUserStorageRequest;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.StorageUser;
import com.cardgame.evolution.DisplayLogic.Room.FindRoom.FindRoomRequest;
import com.cardgame.evolution.DisplayLogic.Room.VoteStart.VoteStartRequest;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoomUser;
import com.cardgame.evolution.Logic.Room.CreateRoom.RoomStatus;
import com.cardgame.evolution.Logic.Room.LeaveRooms.LeaveRoomRequest;
import com.cardgame.evolution.Logic.Room.RoomStorage.GetRoom.GetRoomStorageRequest;
import com.cardgame.evolution.Logic.Room.RoomStorage.GetRoom.StorageRoom;
import com.cardgame.evolution.Logic.Room.RoomStorage.SetRoom.SetRoomStorageRequest;
import com.cardgame.evolution.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RoomHubActivity extends AppCompatActivity {

    private FirebaseObservable<FirebaseRoom> roomObservable;
    private PlayersRecyclerViewAdapter adapter;
    private List<FirebaseRoomUser> roomUsers;
    private StorageUser currentUser;
    private StorageRoom storageRoom;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_hub);
        roomUsers = new ArrayList<>();
        storageRoom = (StorageRoom) Mediator.send(new GetRoomStorageRequest(
                getSharedPreferences(RoomStorage.SHARED_PREFS_NAME, MODE_PRIVATE)));
        currentUser = (StorageUser)Mediator.send(new GetUserStorageRequest(
                getSharedPreferences(UserStorage.SHARED_PREFS_NAME, MODE_PRIVATE)));

        roomObservable = (FirebaseObservable<FirebaseRoom>)Mediator.send(new FindRoomRequest(storageRoom.key));
        Observer observer = (observable, arg) -> {
            if(observable == roomObservable) {
                roomUsers.removeIf(x -> true);
                roomUsers.addAll(roomObservable.getValue().members);
            }
        };

        roomObservable.addObserver(observer);
    }

    @Override
    protected void onStart() {
        super.onStart();
        TextView roomNameView = findViewById(R.id.room_hub_name);
        TextView votedCount = findViewById(R.id.players_voted_textcount);
        roomObservable.addObserver((o, arg) -> {
            if(o == roomObservable) {
                roomNameView.setText(roomObservable.getValue().name);
                votedCount.setText(String.format("Voted for play %d players", roomObservable.getValue().votes));
            }
        });

        roomObservable.addObserver((o, arg) -> {
            if(o == roomObservable && roomObservable.getValue().roomStatus == RoomStatus.Starting) {
                progressDialog = ProgressDialog.show(this, "Starting game", "Starting creating game process");
                Observable finishStartingObserver = new SimpleObservable();
                finishStartingObserver.addObserver((o1, arg1) -> {
                    if (roomObservable.getValue().roomStatus == RoomStatus.InGame) {
                        roomObservable.deleteObservers();
                        Toast.makeText(this, "Game has initialized", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        openGameTable();
                    }
                });

                Mediator.send(new StartGameRequest(
                        storageRoom.key,
                        getSharedPreferences(UserStorage.SHARED_PREFS_NAME, MODE_PRIVATE),
                        progressDialog,
                        finishStartingObserver));
            }
        });

        RecyclerView playersListView = findViewById(R.id.room_hub_players);
        adapter = new PlayersRecyclerViewAdapter(
                roomUsers,
                this);
        playersListView.setLayoutManager(new LinearLayoutManager(this));
        playersListView.setAdapter(adapter);
    }

    protected void onLeaveRoomClick(View view) {
        Mediator.send(new LeaveRoomRequest(currentUser.uid, storageRoom.key));
        Mediator.send(new SetRoomStorageRequest(null, getSharedPreferences(RoomStorage.SHARED_PREFS_NAME, MODE_PRIVATE)));
        onBackPressed();
    }

    public void onVoteClick(View view) {
        Mediator.send(new VoteStartRequest(storageRoom.key, currentUser.uid));
    }

    @Override
    public void onBackPressed() {
        finalizeObservers();
        super.onBackPressed();
    }

    private void openGameTable() {
        finalizeObservers();
        Intent intent = new Intent(this, GameTableActivity.class);
        intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
        startActivity(intent);
    }

    private void finalizeObservers() {
        roomObservable.deleteObservers();
    }
}
