package com.cardgame.evolution.Display.Activities.Game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cardgame.evolution.Common.Bitmap.DrawableHelper;
import com.cardgame.evolution.Common.DisplayHelper;
import com.cardgame.evolution.Display.Activities.Game.DragAndDrop.PlayerEntitiesDragManager;
import com.cardgame.evolution.Entities.Card.Card;
import com.cardgame.evolution.Entities.Property.PropertyHelper;
import com.cardgame.evolution.R;

import java.util.List;

public class EntityRecyclerViewAdapter extends RecyclerView.Adapter<EntityRecyclerViewAdapter.CardPropertiesViewHolder> {
    private List<Card> mDataset;
    private Context context;

    class CardPropertiesViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout propertyLayoutContainer;
        private Point screenSize;
        private LinearLayout.LayoutParams propertyLayoutContainerParams;
        private PlayerEntitiesDragManager dragManager;

        CardPropertiesViewHolder(View itemView) {
            super(itemView);
            propertyLayoutContainer = (LinearLayout)itemView;
            screenSize = DisplayHelper.GetScreenSize(context);

            propertyLayoutContainerParams = new LinearLayout.LayoutParams(
                    screenSize.y / 18,
                    screenSize.y / 18);

            propertyLayoutContainer.setOrientation(LinearLayout.VERTICAL);
            itemView.setPadding(0, 0, 0, 0);
            dragManager = new PlayerEntitiesDragManager();
        }

        void setDetails(Card card) {
            propertyLayoutContainer.removeAllViews();
            propertyLayoutContainer.setOnDragListener(dragManager.getOnDragListener());
            addRow();
            card.properties.forEach(x -> insertProperty(PropertyHelper.getDrawableName(x)));
        }

        private void insertProperty (String drawableName) {
            LinearLayout lastRow = getLastRow();
            if (lastRow.getChildCount() >= 2) {
                addRow();
            }

            insertPropertyInner(drawableName);
        }

        private LinearLayout getLastRow () {
           int rowCount = propertyLayoutContainer.getChildCount();
           if(rowCount == 0) {
               addRow();
           }

           return (LinearLayout)propertyLayoutContainer.getChildAt(rowCount - 1);
        }

        private void addRow() {
            LinearLayout colView = new LinearLayout(context);
            colView.setOrientation(LinearLayout.HORIZONTAL);
            propertyLayoutContainer.addView(colView);
        }

        private void insertPropertyInner (String drawableName) {
            Bitmap propertyIcon = BitmapFactory.decodeResource(
                    context.getResources(),
                    DrawableHelper.getDrawableId(context, drawableName));

            ImageView propertyImage = new ImageView(context);
            propertyImage.setImageBitmap(propertyIcon);
            getLastRow().addView(propertyImage, propertyLayoutContainerParams);
        }
    }

    public EntityRecyclerViewAdapter(
            List<Card> cards,
            Context context) {
        mDataset = cards;
        this.context = context;
    }

    @NonNull
    @Override
    public CardPropertiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item_2, parent, false);
        return new CardPropertiesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardPropertiesViewHolder holder, int position) {
        holder.setDetails(mDataset.get(position));
    }

    public void addItem(Card item) {
        mDataset.add(item);
        notifyItemInserted(mDataset.size() - 1);
    }

    public void removeItem(Card item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemInserted(position);
    }

    public void changeItem(Card item) {
        int position = mDataset.indexOf(mDataset.stream().filter(x -> x.key.equals(item.key)).findAny());
        mDataset.remove(position);
        mDataset.add(position, item);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}