package com.cardgame.evolution.Display.Activities.Game.DragAndDrop;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.DragEvent;
import android.view.View;
import android.widget.Toast;

import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.Display.Activities.Game.TableItemTypes;
import com.cardgame.evolution.Display.Activities.Game.TableViewTag;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.GetUserStorageRequest;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.StorageUser;
import com.cardgame.evolution.DisplayLogic.Mappers.ClipDataEntityDragDropMapper;

public class PlayerEntitiesDragManager {

    private SharedPreferences userStoragePrefs;
    private Context context;

    public View.OnDragListener getOnDragListener() {
        return (v, event) -> {
            if (event.getAction() == DragEvent.ACTION_DROP) {
                boolean isCardFromHand = event.getClipData().getItemAt(0).getText().equals(EntityDragDropData.class.getName());
                if (isCardFromHand) {
                    EntityDragDropData dragDropData = ClipDataEntityDragDropMapper.Map(event.getClipData());
                    if (isValidEventOnPlayer(v, dragDropData.containsBadProp)) {
                        Toast.makeText(context, "Dropped succsessfully", Toast.LENGTH_SHORT).show();
                        ((View) event.getLocalState()).setVisibility(View.INVISIBLE);
                    }
                }
            }
            return true;
        };
    }

    private boolean isValidEventOnPlayer(View target, boolean containsBadProp) {
        if (target.getTag() != null) {
            TableViewTag viewTag = (TableViewTag)target.getTag();
            if (viewTag.type == TableItemTypes.PLAYER) {
                StorageUser storageUser = (StorageUser) Mediator.send(new GetUserStorageRequest(userStoragePrefs));
                assert storageUser != null;
                return storageUser.uid.equals(viewTag.name) || containsBadProp;
            }
        }

        return false;
    }
}
