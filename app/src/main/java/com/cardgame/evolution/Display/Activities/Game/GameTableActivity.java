package com.cardgame.evolution.Display.Activities.Game;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.TableLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.cardgame.evolution.Common.Constants.Login.UserStorage;
import com.cardgame.evolution.Common.Constants.Room.RoomStorage;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.GetUserStorageRequest;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.StorageUser;
import com.cardgame.evolution.Logic.Room.RoomStorage.GetRoom.GetRoomStorageRequest;
import com.cardgame.evolution.Logic.Room.RoomStorage.GetRoom.StorageRoom;
import com.cardgame.evolution.R;

public class GameTableActivity extends AppCompatActivity {

    private boolean isCardsOpened = false;
    private float swipeY1, swipeY2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_table);
    }

    @Override
    protected void onStart () {
        super.onStart();
        StorageRoom storageRoom = (StorageRoom)Mediator.send(
                new GetRoomStorageRequest(getSharedPreferences(RoomStorage.SHARED_PREFS_NAME, MODE_PRIVATE)));
        StorageUser storageUser = (StorageUser)Mediator.send(
                new GetUserStorageRequest(getSharedPreferences(UserStorage.SHARED_PREFS_NAME, MODE_PRIVATE)));

        TableLayout gameTable = findViewById(R.id.game_table);
        TableInitializer tableInitializer = new TableInitializer(
                gameTable,
                findViewById(R.id.game_table_constraint_container),
                this,
                findViewById(R.id.game_table_hand_cards));
        tableInitializer.setupPlayers(storageRoom.key, storageUser.uid);
        gameTable.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    swipeY1 = event.getY();
                    break;
                case MotionEvent.ACTION_UP:
                    swipeY2 = event.getY();
                    float deltaX = swipeY2 - swipeY1;
                    if (Math.abs(deltaX) > 150)
                    {
                        toggleHandCards();
                    }
                    break;
            }

            return true;
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        //finalizeObservers();
        super.onBackPressed();
    }

    public void toggleCardsClick(View view) {
        toggleHandCards();
    }

    private void toggleHandCards() {
        LinearLayout tableContainer = findViewById(R.id.game_table_container);
        final int oldTopMargin = isCardsOpened ? -300 : 0;
        final int oldBottomMargin = isCardsOpened ? 300 : 0;
        final int newTopMargin = isCardsOpened ? 0 : -300;
        final int newBottomMargin = isCardsOpened ? 0 : 300;
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) tableContainer.getLayoutParams();
                params.topMargin = oldTopMargin + (int)((newTopMargin - oldTopMargin) * interpolatedTime);
                params.bottomMargin = oldBottomMargin + (int)((newBottomMargin - oldBottomMargin) * interpolatedTime);
                tableContainer.setLayoutParams(params);
            }
        };

        animation.setDuration(300);
        tableContainer.startAnimation(animation);

        isCardsOpened = !isCardsOpened;
    }
}
