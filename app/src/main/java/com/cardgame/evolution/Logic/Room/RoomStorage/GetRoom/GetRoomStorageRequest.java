package com.cardgame.evolution.Logic.Room.RoomStorage.GetRoom;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = GetRoomStorageHandler.class)
public class GetRoomStorageRequest extends Request {
    public GetRoomStorageRequest(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public SharedPreferences sharedPreferences;
}
