package com.cardgame.evolution.Logic.Login.Telegram;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = GenerateTelegramLoginRequestHandler.class)
public class GenerateTelegramLoginRequest extends Request {
}
