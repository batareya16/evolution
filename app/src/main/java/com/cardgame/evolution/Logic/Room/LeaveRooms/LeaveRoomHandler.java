package com.cardgame.evolution.Logic.Room.LeaveRooms;

import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;
import com.cardgame.evolution.Logic.Room.RoomStorage.SetRoom.SetRoomStorageRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Observable;
import java.util.Observer;

public class LeaveRoomHandler implements Handleable<LeaveRoomRequest, Object> {
    @Override
    public Object handle(LeaveRoomRequest model) {
        DatabaseReference roomRef = FirebaseDatabase.getInstance().getReference()
                .child("rooms").child(model.roomKey);

        FirebaseObservable<FirebaseRoom> roomObservable = new FirebaseObservable<>(roomRef, FirebaseRoom.class);
        roomObservable.addObserver((o, arg) -> {
            if (o == roomObservable) {
                roomObservable.deleteObservers();
                FirebaseRoom room = roomObservable.getValue();
                if (room != null) {
                    if (room.members.size() > 1) {
                        room.members.removeIf(x -> x.uid.equals(model.userUid));
                        roomObservable.setValue(room);
                    }
                    else {
                        roomObservable.removeValue();
                    }
                }
            }
        });

        return null;
    }
}
