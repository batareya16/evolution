package com.cardgame.evolution.Logic.Room.CreateRoom;

import com.cardgame.evolution.Common.FirebaseReference;
import com.cardgame.evolution.Entities.GameProcess.Game;

import java.util.Date;
import java.util.List;

public class FirebaseRoom extends FirebaseReference {

    public FirebaseRoom(
            String name,
            String password,
            Date createdDate,
            int maxPlayers,
            List<FirebaseRoomUser> members) {
        this.name = name;
        this.password = password;
        this.createdDate = createdDate;
        this.members = members;
        this.maxPlayers = maxPlayers;
    }

    public FirebaseRoom() {
    }

    public String name;
    public String password;
    public Date createdDate;
    public List<FirebaseRoomUser> members;
    public int maxPlayers;
    public int votes;
    public String host;
    public RoomStatus roomStatus;
    public Game game;
}
