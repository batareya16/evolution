package com.cardgame.evolution.Logic.Storage.SharedPrefs.Pop;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = PopStringHandler.class)
public class PopStringPrefsRequest extends Request {

    public PopStringPrefsRequest(SharedPreferences sharedPreferences, String keyString) {
        this.sharedPreferences = sharedPreferences;
        this.keyString = keyString;
    }

    SharedPreferences sharedPreferences;

    String keyString;
}
