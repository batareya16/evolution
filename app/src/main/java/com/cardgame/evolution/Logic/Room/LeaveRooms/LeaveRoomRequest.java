package com.cardgame.evolution.Logic.Room.LeaveRooms;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.StorageUser;

@ResolveMediator(handler = LeaveRoomHandler.class)
public class LeaveRoomRequest extends Request {
    public LeaveRoomRequest(String userUid, String roomKey) {
        this.userUid = userUid;
        this.roomKey = roomKey;
    }

    public String userUid;
    public String roomKey;
}
