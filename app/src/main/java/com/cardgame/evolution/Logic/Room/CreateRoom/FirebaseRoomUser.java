package com.cardgame.evolution.Logic.Room.CreateRoom;

public class FirebaseRoomUser {
    public FirebaseRoomUser(String uid, String userName, String email, String photoUri) {
        this.uid = uid;
        this.userName = userName;
        this.email = email;
        this.photoUri = photoUri;
    }

    public FirebaseRoomUser() {
    }

    public boolean voted;
    public String uid;
    public String userName;
    public String email;
    public String photoUri;
}
