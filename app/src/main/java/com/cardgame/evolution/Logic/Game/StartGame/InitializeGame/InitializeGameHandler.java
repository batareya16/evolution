package com.cardgame.evolution.Logic.Game.StartGame.InitializeGame;

import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Common.Shuffler;
import com.cardgame.evolution.Entities.Card.PackCreator;
import com.cardgame.evolution.Entities.GameProcess.Game;
import com.cardgame.evolution.Entities.GameProcess.GameProcess;
import com.cardgame.evolution.Entities.Player.Player;
import com.cardgame.evolution.Entities.Player.PlayerHand;
import com.cardgame.evolution.Entities.Player.PlayerHandHelper;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;
import com.cardgame.evolution.Logic.Room.CreateRoom.RoomStatus;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class InitializeGameHandler implements Handleable<InitializeGameRequest, Object> {
    @Override
    public Object handle(InitializeGameRequest model) {
        Game game = new Game();
        game.gameProcess = new GameProcess();
        game.gameProcess.cardPack = Shuffler.shuffleCards(PackCreator.getStandartCardsPack());
        model.roomObservable.addObserver((o, arg) -> {
            FirebaseRoom room = model.roomObservable.getValue();
            game.players = room.members.stream().map(Player::new).collect(Collectors.toList());
            setupPlayersHands(game);
            room.game = game;
            room.roomStatus = RoomStatus.InGame;
            model.roomObservable.setValue(room);
        });


        return null;
    }

    private void setupPlayersHands(Game game) {
        for (Player player : game.players) {
            player.hand = new PlayerHand();
            player.hand.cards = new ArrayList<>();
            for (int i = 0; i < 6; i++) {
                PlayerHandHelper.takeCard(game, player);
            }
        }
    }
}
