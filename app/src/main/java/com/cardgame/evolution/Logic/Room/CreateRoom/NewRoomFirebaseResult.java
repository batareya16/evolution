package com.cardgame.evolution.Logic.Room.CreateRoom;

import com.google.android.gms.tasks.Task;

public class NewRoomFirebaseResult {

    public NewRoomFirebaseResult(String key, Task<Void> addRoomPromise) {
        this.key = key;
        this.addRoomPromise = addRoomPromise;
    }

    public String key;
    public Task<Void> addRoomPromise;
}
