package com.cardgame.evolution.Logic.Login.Telegram;

import com.cardgame.evolution.Common.Constants.Login.TelegramAuth;
import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Common.RandomGenerator;

import org.telegram.passport.PassportScope;
import org.telegram.passport.PassportScopeElementOne;
import org.telegram.passport.TelegramPassport;

public class GenerateTelegramLoginRequestHandler implements Handleable<GenerateTelegramLoginRequest, TelegramPassport.AuthRequest> {

    @Override
    public TelegramPassport.AuthRequest handle(GenerateTelegramLoginRequest model) {
        TelegramPassport.AuthRequest request = new TelegramPassport.AuthRequest();
        request.botID = TelegramAuth.BOT_ID;
        request.publicKey = TelegramAuth.BOT_PUBLIC_KEY;
        request.nonce = RandomGenerator.generateSecureString(TelegramAuth.NONCE_SYMBOLS);
        request.scope = new PassportScope(new PassportScopeElementOne(PassportScope.EMAIL));
        return request;
    }
}
