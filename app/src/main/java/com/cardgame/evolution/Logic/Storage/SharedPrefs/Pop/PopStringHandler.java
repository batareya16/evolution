package com.cardgame.evolution.Logic.Storage.SharedPrefs.Pop;

import com.cardgame.evolution.Common.Mediator.Handleable;

public class PopStringHandler implements Handleable<PopStringPrefsRequest, String> {

    @Override
    public String handle(PopStringPrefsRequest model) {
        return model.sharedPreferences.getString(model.keyString, null);
    }
}
