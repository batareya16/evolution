package com.cardgame.evolution.Logic.Game.StartGame.SelectHost;

import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;
import com.cardgame.evolution.DisplayLogic.Game.StartGame.StartGameTimerParams;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;

import java.util.List;

@ResolveMediator(handler = SelectHostHandler.class)
public class SelectHostRequest extends Request {

    public SelectHostRequest(
            List<String> membersIds,
            FirebaseObservable<FirebaseRoom> roomObservable,
            StartGameTimerParams gameTimerParams) {
        this.membersIds = membersIds;
        this.roomObservable = roomObservable;
        this.gameTimerParams = gameTimerParams;
    }

    public List<String> membersIds;
    public FirebaseObservable<FirebaseRoom> roomObservable;
    public StartGameTimerParams gameTimerParams;
}
