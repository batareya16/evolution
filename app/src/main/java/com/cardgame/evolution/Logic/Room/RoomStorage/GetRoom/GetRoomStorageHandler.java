package com.cardgame.evolution.Logic.Room.RoomStorage.GetRoom;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Constants.Room.RoomStorage;
import com.cardgame.evolution.Common.Mediator.Handleable;

import static com.cardgame.evolution.Common.StorageHelper.pop;

public class GetRoomStorageHandler implements Handleable<GetRoomStorageRequest, StorageRoom> {
    @Override
    public StorageRoom handle(GetRoomStorageRequest model) {
        SharedPreferences sharedPreferences = model.sharedPreferences;
        return new StorageRoom(
                pop(sharedPreferences, RoomStorage.ROOM_KEY)
        );
    }
}
