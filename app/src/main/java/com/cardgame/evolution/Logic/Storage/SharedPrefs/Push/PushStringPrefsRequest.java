package com.cardgame.evolution.Logic.Storage.SharedPrefs.Push;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = PushStringPrefsHandler.class)
public class PushStringPrefsRequest extends Request {

    public PushStringPrefsRequest(SharedPreferences sharedPreferences, String keyString, String valueString) {
        this.sharedPreferences = sharedPreferences;
        this.keyString = keyString;
        this.valueString = valueString;
    }

    SharedPreferences sharedPreferences;

    String keyString;

    String valueString;
}
