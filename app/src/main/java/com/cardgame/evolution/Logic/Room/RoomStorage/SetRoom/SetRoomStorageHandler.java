package com.cardgame.evolution.Logic.Room.RoomStorage.SetRoom;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Constants.Room.RoomStorage;
import com.cardgame.evolution.Common.Mediator.Handleable;

import static com.cardgame.evolution.Common.StorageHelper.push;

public class SetRoomStorageHandler implements Handleable<SetRoomStorageRequest, Object> {
    @Override
    public Object handle(SetRoomStorageRequest model) {
        SharedPreferences sharedPreferences = model.sharedPreferences;
        push(sharedPreferences, RoomStorage.ROOM_KEY, model.roomKey);
        return null;
    }
}
