package com.cardgame.evolution.Logic.Room.CreateRoom;

import com.cardgame.evolution.Common.Mediator.Handleable;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewRoomFirebaseHandler implements Handleable<NewRoomFirebaseRequest, NewRoomFirebaseResult> {
    @Override

    public NewRoomFirebaseResult handle(NewRoomFirebaseRequest model) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        List<FirebaseRoomUser> members = new ArrayList<>();
        members.add(new FirebaseRoomUser(model.hostUser.uid, model.hostUser.name, model.hostUser.email, model.hostUser.photoUri));
        FirebaseRoom room = new FirebaseRoom(
                model.name,
                model.password,
                new Date(),
                model.maxPlayers,
                members
        );

        String key = database.child("rooms").push().getKey();
        Map<String, Object> roomUpdates = new HashMap<>();
        roomUpdates.put("/rooms/" + key, room);
        Task<Void> promise = database.updateChildren(roomUpdates);
        return new NewRoomFirebaseResult(key, promise);
    }
}
