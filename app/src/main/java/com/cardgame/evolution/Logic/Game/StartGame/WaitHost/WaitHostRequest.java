package com.cardgame.evolution.Logic.Game.StartGame.WaitHost;

import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;
import com.cardgame.evolution.DisplayLogic.Game.StartGame.StartGameTimerParams;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;

import java.util.List;

@ResolveMediator(handler = WaitHostHandler.class)
public class WaitHostRequest extends Request {

    public WaitHostRequest(
            StartGameTimerParams gameTimerParams,
            FirebaseObservable<FirebaseRoom> roomObservable,
            List<String> membersIds) {
        this.gameTimerParams = gameTimerParams;
        this.roomObservable = roomObservable;
        this.membersIds = membersIds;
    }

    public StartGameTimerParams gameTimerParams;
    public FirebaseObservable<FirebaseRoom> roomObservable;
    public List<String> membersIds;
}
