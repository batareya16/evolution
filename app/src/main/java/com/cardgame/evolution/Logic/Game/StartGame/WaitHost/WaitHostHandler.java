package com.cardgame.evolution.Logic.Game.StartGame.WaitHost;

import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Common.Mediator.Mediator;
import com.cardgame.evolution.Logic.Game.StartGame.SelectHost.SelectHostRequest;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;

import java.util.Timer;
import java.util.TimerTask;

public class WaitHostHandler implements Handleable<WaitHostRequest, Object> {
    @Override
    public Object handle(WaitHostRequest model) {
        if (model.gameTimerParams.waitHostTimer != null) {
            model.gameTimerParams.waitHostTimer.cancel();
        }

        model.gameTimerParams.waitHostTimer = new Timer(true);
        model.gameTimerParams.waitHostTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                FirebaseRoom room = model.roomObservable.getValue();

                if (model.gameTimerParams.currentHostIndex == room.members.size() - 1) {
                    throw new IndexOutOfBoundsException();
                }

                if(room.host.equals(model.gameTimerParams.prevHost)) {
                    Mediator.send(new SelectHostRequest(model.membersIds, model.roomObservable, model.gameTimerParams));
                }
            }
        }, 15000);

        return null;
    }
}
