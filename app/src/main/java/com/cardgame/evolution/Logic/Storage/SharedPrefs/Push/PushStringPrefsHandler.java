package com.cardgame.evolution.Logic.Storage.SharedPrefs.Push;

import android.content.Context;
import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Mediator.Handleable;

public class PushStringPrefsHandler implements Handleable<PushStringPrefsRequest, Object> {

    @Override
    public Object handle(PushStringPrefsRequest model) {
        SharedPreferences.Editor editor = model.sharedPreferences.edit();
        editor.putString(model.keyString, model.valueString);
        editor.commit();
        return null;
    }
}
