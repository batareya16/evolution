package com.cardgame.evolution.Logic.Game.StartGame.InitializeGame;

import com.cardgame.evolution.Common.FirebaseObservable;
import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;

@ResolveMediator(handler = InitializeGameHandler.class)
public class InitializeGameRequest extends Request {
    public InitializeGameRequest(FirebaseObservable<FirebaseRoom> roomObservable) {
        this.roomObservable = roomObservable;
    }

    FirebaseObservable<FirebaseRoom> roomObservable;
}
