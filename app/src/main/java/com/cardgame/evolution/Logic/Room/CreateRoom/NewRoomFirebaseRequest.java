package com.cardgame.evolution.Logic.Room.CreateRoom;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;
import com.cardgame.evolution.DisplayLogic.Login.UserStorage.GetUser.StorageUser;

@ResolveMediator(handler = NewRoomFirebaseHandler.class)
public class NewRoomFirebaseRequest extends Request {

    public NewRoomFirebaseRequest(
            String name,
            String password,
            StorageUser hostUser,
            int maxPlayers) {
        this.name = name;
        this.password = password;
        this.hostUser = hostUser;
        this.maxPlayers = maxPlayers;
    }

    String name;
    String password;
    StorageUser hostUser;
    int maxPlayers;
}
