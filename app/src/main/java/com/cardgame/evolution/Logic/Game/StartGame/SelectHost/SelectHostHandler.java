package com.cardgame.evolution.Logic.Game.StartGame.SelectHost;

import com.cardgame.evolution.Common.Mediator.Handleable;
import com.cardgame.evolution.Logic.Room.CreateRoom.FirebaseRoom;

public class SelectHostHandler implements Handleable<SelectHostRequest, Object> {
    @Override
    public Object handle(SelectHostRequest model) {
        FirebaseRoom room = model.roomObservable.getValue();
        room.host = model.membersIds.get(++model.gameTimerParams.currentHostIndex);
        model.roomObservable.setValue(room);

        return null;
    }
}
