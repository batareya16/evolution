package com.cardgame.evolution.Logic.Room.CreateRoom;

public enum RoomStatus {
    Joining,
    Starting,
    InGame
}
