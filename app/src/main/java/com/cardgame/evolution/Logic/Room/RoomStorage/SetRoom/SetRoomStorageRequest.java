package com.cardgame.evolution.Logic.Room.RoomStorage.SetRoom;

import android.content.SharedPreferences;

import com.cardgame.evolution.Common.Mediator.Request;
import com.cardgame.evolution.Common.Mediator.ResolveMediator;

@ResolveMediator(handler = SetRoomStorageHandler.class)
public class SetRoomStorageRequest extends Request {

    public SetRoomStorageRequest(String roomKey, SharedPreferences sharedPreferences) {
        this.roomKey = roomKey;
        this.sharedPreferences = sharedPreferences;
    }


    public String roomKey;
    public SharedPreferences sharedPreferences;
}
